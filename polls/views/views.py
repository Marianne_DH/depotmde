""" Cornice services.
"""
from cornice import Service
from datetime import datetime

hello = Service(name='hello', path='/', description="Simplest app")
time = Service(name='time', path='/time', description="Get current time")

@hello.get()
def get_info(request):
    """Returns Hello in JSON."""
    return {'Hello': 'World'}

@time.get()
def get_time(request):
	"""Returns current time in String"""
	date_str = str(datetime.now())
	return date_str

@hello.post()
def post_name(request):
	"""Returns user name in String"""
	print(request)
	user_name = request.POST['param1']
	text_displayed = "Bonjour tu t'appel: %s" % (user_name)
	return text_displayed
